// import files
var sgpa = require('./calculator/sgpa')
var percentage = require('./calculator/percentage')
var grade = require('./calculator/grade')
var performance = require('./calculator/performance')

// Test Of 1 Student Record
// Test Case 1 (SGPA)
test("Should Get Correct SGPA Score", () => {
  expect(sgpa(21,30,32,28,36,20,9)).toBe("8.38");
});
// Test Case 2 (Percentage )
test("Should Get Correct Percentage", () => {
  expect(percentage(21,30,32,28,36,20,9)).toBe("76.3");
});
// Test Case 3 (Grade)
test("Should Get Correct Grade", () => {
  expect(grade(21,30,32,28,36,20,9)).toBe("B");
});
// Test Case 4 (Performance)
test("Should Get Correct Performance", () => {
  expect(performance(21,30,32,28,36,20,9)).toBe("Very Good");
});

// Test Of 2 Student Record
// Test Case 1 (SGPA)
test("Should Get Correct SGPA Score", () => {
  expect(sgpa(21,30,36,36,40,20,9)).toBe("9.14");
});
// Test Case 2 (Percentage )
test("Should Get Correct Percentage", () => {
  expect(percentage(21,30,36,36,40,20,9)).toBe("83.9");
});
// Test Case 3 (Grade)
test("Should Get Correct Grade", () => {
  expect(grade(21,30,36,36,40,20,9)).toBe("A");
});
// Test Case 4 (Performance)
test("Should Get Correct Performance", () => {
  expect(performance(21,30,36,36,40,20,9)).toBe("Excellent");
});
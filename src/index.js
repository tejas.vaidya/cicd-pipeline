const calculate = () =>{
  let ps = document.getElementById('1').value;
  let dsgt = document.getElementById('2').value;
  let ds = document.getElementById('3').value;
  let cao = document.getElementById('4').value;
  let dbms = document.getElementById('5').value;
  let seva = document.getElementById('6').value;
  let hss = document.getElementById('7').value;
  let grades = "";
  let performance ="";


  let totalGrades = parseFloat(ps) + parseFloat(dsgt) + parseFloat(ds) + parseFloat(cao) + parseFloat(dbms) + parseFloat(seva) + parseFloat(hss);
//   alert(totalGrades);

  let SGPA = (totalGrades/21).toFixed(2);
//   alert(SGPA);

  let perc = ((SGPA-0.75)*10).toFixed(1);
//   alert(perc);

  debugger;

  if(SGPA >= 10){
    grades = 'O';
    performance = 'Outstanding';
  }else if(SGPA >=9){
     grades = 'A';
     performance = 'Excellent';
  }else if(SGPA >= 8){
     grades = 'B';
     performance = 'Very Good';
  }else if(SGPA >= 7){
     grades = 'C';
     performance = 'Good';
  }else if(SGPA >= 6){
     grades = 'D';
     performance = 'Fair';
  }else if(SGPA >= 5){
     grades = 'E';
     performance = 'Average';
  }else if(SGPA >= 4){
     grades = 'P';
     performance = 'Pass';
  }else{
     grades = 'F';
     performance = 'Fail';
  }
  document.getElementById('showData').innerHTML = ` Out of 210 your total is  <span style="color: #33d9b2">${totalGrades}</span>, SGPA is <span style="color: #33d9b2">${SGPA}</span> and percentage is <span style="color: #33d9b2">${perc}%</span>. <br> Your grade is <span style="color: #33d9b2">${grades}</span>. You are Performance is <span style="color: #33d9b2">${performance}</span>.`
}
module.exports = calculate
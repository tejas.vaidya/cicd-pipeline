/**
 * Multiplication
 */
 function grade(ps,dsgt,ds,cao,dbms,seva,hss) {
  let grades = "";
  let performance ="";
  let totalGrades = parseFloat(ps) + parseFloat(dsgt) + parseFloat(ds) + parseFloat(cao) + parseFloat(dbms) + parseFloat(seva) + parseFloat(hss);
  //   alert(totalGrades);
  
    let SGPA = (totalGrades/21).toFixed(2);
  //   alert(SGPA);

    let perc = ((SGPA-0.75)*10).toFixed(1);
  //   alert(perc);
  
    debugger;
  
    if(SGPA >= 10){
      grades = 'O';
      performance = 'Outstanding';
    }else if(SGPA >=9){
       grades = 'A';
       performance = 'Excellent';
    }else if(SGPA >= 8){
       grades = 'B';
       performance = 'Very Good';
    }else if(SGPA >= 7){
       grades = 'C';
       performance = 'Good';
    }else if(SGPA >= 6){
       grades = 'D';
       performance = 'Fair';
    }else if(SGPA >= 5){
       grades = 'E';
       performance = 'Average';
    }else if(SGPA >= 4){
       grades = 'P';
       performance = 'Pass';
    }else{
       grades = 'F';
       performance = 'Fail';
    }
    return grades;
}
  
  module.exports = grade
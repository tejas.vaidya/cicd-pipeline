/*
 * ATTENTION: The "eval" devtool has been used (maybe by default in mode: "development").
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
/******/ (() => { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/***/ ((module) => {

eval("const calculate = () => {\n  let ps = document.getElementById('1').value;\n  let dsgt = document.getElementById('2').value;\n  let ds = document.getElementById('3').value;\n  let cao = document.getElementById('4').value;\n  let dbms = document.getElementById('5').value;\n  let seva = document.getElementById('6').value;\n  let hss = document.getElementById('7').value;\n  let grades = \"\";\n  let performance = \"\";\n  let totalGrades = parseFloat(ps) + parseFloat(dsgt) + parseFloat(ds) + parseFloat(cao) + parseFloat(dbms) + parseFloat(seva) + parseFloat(hss);\n  //   alert(totalGrades);\n\n  let SGPA = (totalGrades / 21).toFixed(2);\n  //   alert(SGPA);\n\n  let perc = ((SGPA - 0.75) * 10).toFixed(1);\n  //   alert(perc);\n\n  debugger;\n  if (SGPA >= 10) {\n    grades = 'O';\n    performance = 'Outstanding';\n  } else if (SGPA >= 9) {\n    grades = 'A';\n    performance = 'Excellent';\n  } else if (SGPA >= 8) {\n    grades = 'B';\n    performance = 'Very Good';\n  } else if (SGPA >= 7) {\n    grades = 'C';\n    performance = 'Good';\n  } else if (SGPA >= 6) {\n    grades = 'D';\n    performance = 'Fair';\n  } else if (SGPA >= 5) {\n    grades = 'E';\n    performance = 'Average';\n  } else if (SGPA >= 4) {\n    grades = 'P';\n    performance = 'Pass';\n  } else {\n    grades = 'F';\n    performance = 'Fail';\n  }\n  document.getElementById('showData').innerHTML = ` Out of 210 your total is  <span style=\"color: #33d9b2\">${totalGrades}</span>, SGPA is <span style=\"color: #33d9b2\">${SGPA}</span> and percentage is <span style=\"color: #33d9b2\">${perc}%</span>. <br> Your grade is <span style=\"color: #33d9b2\">${grades}</span>. You are Performance is <span style=\"color: #33d9b2\">${performance}</span>.`;\n};\nmodule.exports = calculate;\n\n//# sourceURL=webpack://cicd-pipeline/./src/index.js?");

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module is referenced by other modules so it can't be inlined
/******/ 	var __webpack_exports__ = __webpack_require__("./src/index.js");
/******/ 	
/******/ })()
;